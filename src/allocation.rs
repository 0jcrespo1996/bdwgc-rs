use std::alloc::{GlobalAlloc, Layout};
use std::ffi::c_void;
use bdwgc_sys::*;
use crate::error::*;
use crate::handle::*;

pub trait AssertSized: Sized { type Itself; }

impl<T> AssertSized for T { type Itself = Self; }

pub type InstanceFinalizer<T, TData = RawType> = extern "C" fn(handle: GcHandle<T>, data: *mut <TData as AssertSized>::Itself);
pub type GcStats = GC_prof_stats_s;


/// A trait for types that can be initialized after allocation.
pub trait GcInitialize
{
    /// Initialize the object.
    fn initialize(&mut self);
}

pub fn initialize_gc() {
    unsafe { GC_init() };
}


/// The mode of allocation.
pub enum AllocateMode
{
    /// Allocates and zero-initializes the object.
    Normal,

    /// Allocates the object, zero-initializes it, but marks it as uncollectable.
    Uncollected,

    IgnoreOffPage,

    /// Allocates the object, but does not zero-initialize it.
    Atomic,

    /// Allocates the object, but does not zero-initialize it, and marks it as uncollectable.
    AtomicUncollected,

    AtomicIgnoreOffPage,
}

/// Allocates `size` zeroed bytes of memory.
/// * `size`: The size of the allocation in bytes.
/// * `mode`: The allocation mode.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let handle = allocate_raw(4usize, AllocateMode::Normal);
/// assert!(!handle.unwrap().is_null());
/// ```
///
/// # Errors
/// `ErrorKind::OutOfMemory`: Not enough memory to allocate the requested size.
/// `ErrorKind::InvalidArgument`: The requested size is invalid.
///

pub fn allocate_raw(size: usize, mode: AllocateMode) -> Result<RawHandle, Error> {
    if size == 0 {
        return Err(Error::Custom(ErrorKind::InvalidArgument, "Size must be greater than 0"));
    }
    unsafe {
        let ptr = match mode {
            AllocateMode::Normal => GC_malloc(size as size_t),
            AllocateMode::Atomic => GC_malloc_atomic(size as size_t),
            AllocateMode::Uncollected => GC_malloc_uncollectable(size as size_t),
            AllocateMode::IgnoreOffPage => GC_malloc_ignore_off_page(size as size_t),
            AllocateMode::AtomicUncollected => GC_malloc_atomic_uncollectable(size as size_t),
            AllocateMode::AtomicIgnoreOffPage => GC_malloc_atomic_ignore_off_page(size as size_t),
        } as *mut c_void;
        if ptr.is_null() {
            return Err(Error::Simple(ErrorKind::OutOfMemory));
        }

        Ok(GcHandle::from_ptr(ptr))
    }
}


/// Allocates `T` size zeroed bytes of memory.
/// * `T`: The type to allocate.
/// * `mode`: The allocation mode.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let mut handle = allocate::<u32>(AllocateMode::Normal).unwrap();
/// *handle = 502;
/// assert_eq!(*handle, 502);
/// ```
///
/// # Errors
/// `ErrorKind::OutOfMemory`: Not enough memory to allocate the requested size.
/// `ErrorKind::InvalidArgument`: The size of the requested type is invalid.
pub fn allocate<T: Sized>(mode: AllocateMode) -> Result<GcHandle<T>, Error> {
    let handle = allocate_raw(std::mem::size_of::<T>(), mode)?;
    Ok(handle.cast())
}


/// Allocates `T` size zeroed bytes of memory.
/// * `T`: The type to allocate.
/// * `mode`: The allocation mode.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
///
/// struct Foo {
///     x: u32
/// }
///
/// impl GcInitialize for Foo {
///     fn initialize(&mut self) {
///        self.x = 502;
///    }
/// }
///
/// let handle = allocate_init::<Foo>(AllocateMode::Normal).unwrap();
/// assert_eq!(handle.x, 502)
/// ```
pub fn allocate_init<T: Sized + GcInitialize>(mode: AllocateMode) -> Result<GcHandle<T>, Error> {
    let mut handle = allocate::<T>(mode)?;
    handle.initialize();
    Ok(handle)
}


/// Allocates `T` and stores `value` in it.
/// * `T`: The type to allocate.
/// * `value`: The value to store in the allocated object.
/// * `mode`: The allocation mode.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let handle = allocate_value(502u32, AllocateMode::Normal).unwrap();
/// assert_eq!(*handle, 502);
/// ```
pub fn allocate_value<T: Sized>(value: T, mode: AllocateMode) -> Result<GcHandle<T>, Error> {
    let mut handle = allocate::<T>(mode)?;
    *handle = value;
    Ok(handle)
}


/// Reallocates `size` bytes of memory.
/// * `handle`: The handle to the memory to reallocate.
/// * `size`: The of the new allocation in bytes.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let handle = allocate_raw(4usize, AllocateMode::Normal).unwrap();
/// assert!(!handle.is_null());
/// let handle = reallocate_raw(handle, 8usize).unwrap();
/// assert!(!handle.is_null());
/// ```
pub fn reallocate_raw(handle: RawHandle, new_size: usize) -> Result<RawHandle, Error> {
    unsafe {
        let ptr = GC_realloc(handle.as_ptr() as *mut c_void, new_size as size_t);
        if ptr.is_null() {
            return Err(Error::Simple(ErrorKind::OutOfMemory));
        }
        Ok(GcHandle::from_ptr(ptr))
    }
}

/// Reallocates `T` size bytes of memory
/// * `T`: The allocated type.
/// * `handle`: The handle to the memory to reallocate.
/// * `size`: The size of the new allocation in bytes.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let mut handle = allocate::<u32>(AllocateMode::Normal).unwrap();
/// *handle = 502;
/// assert_eq!(*handle, 502);
/// let handle = reallocate::<u32>(handle, 8usize).unwrap();
/// assert_eq!(*handle, 502);
/// ```
///
pub fn reallocate<T: Sized>(handle: GcHandle<T>, new_size: usize) -> Result<GcHandle<T>, Error> {
    let ptr = reallocate_raw(handle.cast(), new_size)?;
    Ok(ptr.cast())
}


/// Frees the memory allocated by `handle`.
/// * `handle`: The handle to free.
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// let handle = allocate::<u32>(AllocateMode::Normal).unwrap();
/// free(handle);
/// ```
///
pub fn free<T: Sized>(handle: GcHandle<T>) {
    if handle.is_null() {
        return;
    }

    unsafe {
        GC_free(handle.as_ptr() as *mut c_void);
    }
}


pub enum FinalizerMode
{
    Normal,
    IgnoreSelf,
    NoOrdering,
    Unreachable,
}

pub const DEFAULT_FINALIZER_MODE: FinalizerMode = FinalizerMode::IgnoreSelf;

/// Sets the `finalizer` for the provided `handle`.
/// * `handle`: The handle to set the finalizer for.
/// * `finalizer`: The finalizer to set.
///
///
/// # example
/// ```
/// use bdwgc_rs::allocation::*;
/// use bdwgc_rs::handle::GcHandle;
/// extern "C" fn finalizer(handle: GcHandle<u32>, data: *mut bool){
///     println!("Finalizing handle: {:?}", handle);
///     unsafe {*data = true};
/// }
///
/// let handle = allocate::<u32>(AllocateMode::Normal).unwrap();
/// let mut invoked = false;
/// set_finalizer::<u32, bool>(handle, Some(finalizer), Some(&mut invoked), DEFAULT_FINALIZER_MODE);
/// ```
///
pub fn set_finalizer<T: Sized, TData: Sized>(handle: GcHandle<T>, finalizer: Option<InstanceFinalizer<T, TData>>, data: Option<*mut TData>, mode: FinalizerMode) -> Result<(), Error> {
    unsafe {
        let ptr: *mut T = handle.try_into()?;
        let data_ptr = data.unwrap_or(std::ptr::null_mut()) as *mut c_void;
        let finalizer_ptr = finalizer.map(|f| std::mem::transmute(f));
        match mode {
            FinalizerMode::IgnoreSelf => GC_register_finalizer_ignore_self(ptr as *mut c_void, finalizer_ptr, data_ptr, std::ptr::null_mut(), std::ptr::null_mut()),
            FinalizerMode::Normal => GC_register_finalizer(ptr as *mut c_void, finalizer_ptr, data_ptr, std::ptr::null_mut(), std::ptr::null_mut()),
            FinalizerMode::NoOrdering => GC_register_finalizer_no_order(ptr as *mut c_void, finalizer_ptr, data_ptr, std::ptr::null_mut(), std::ptr::null_mut()),
            FinalizerMode::Unreachable => GC_register_finalizer_unreachable(ptr as *mut c_void, finalizer_ptr, data_ptr, std::ptr::null_mut(), std::ptr::null_mut()),
        };
    }
    Ok(())
}

pub fn get_gc_stats() -> GcStats {
    unsafe {
        let mut buffer = [0u8; std::mem::size_of::<GcStats>()];
        GC_get_prof_stats(buffer.as_mut_ptr() as *mut GcStats, buffer.len() as size_t);
        *(buffer.as_ptr() as *const GcStats)
    }
}

pub fn force_collection() {
    unsafe {
        GC_gcollect();
    }
}

pub fn get_heap_size() -> usize {
    unsafe {
        GC_get_heap_size() as usize
    }
}


pub fn set_max_heap_size(size: usize) -> Result<(), Error> {
    if size < u16::MAX as usize && size <= usize::MAX {
        return Err(Error::Custom(ErrorKind::InvalidArgument, "size must be greater than u16::MAX and less than usize::MAX"));
    }
    unsafe {
        GC_set_max_heap_size(size as size_t);
    }
    Ok(())
}

pub struct GcAllocator;

unsafe impl GlobalAlloc for GcAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        allocate_raw(layout.size(), AllocateMode::Atomic).unwrap().as_ptr() as *mut u8
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        GC_free(ptr as *mut c_void)
    }

    unsafe fn realloc(&self, ptr: *mut u8, _layout: Layout, new_size: usize) -> *mut u8 {
        GC_realloc(ptr as *mut c_void, new_size as size_t) as *mut u8
    }
}

impl Default for GcAllocator
{
    fn default() -> Self
    {
        initialize_gc();
        GcAllocator
    }
}