

#[derive(Debug, Copy, Clone)]
pub enum ErrorKind
{
    InvalidArgument,
    OutOfMemory,
    NullPointer,
}

#[derive(Debug, Copy, Clone)]
pub enum Error
{
    Simple(ErrorKind),
    Custom(ErrorKind, &'static str)
}