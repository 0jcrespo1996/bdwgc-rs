use std::ffi::c_void;
use std::fmt::{Debug, Formatter};
use std::ops::{Deref, DerefMut, Index, IndexMut};

use crate::error::{Error, ErrorKind};

pub type RawType = c_void;
pub type RawHandle = GcHandle<RawType>;

/// Thin wrapper around a GC managed pointer.
#[derive(PartialEq, Eq)]
#[repr(transparent)]
pub struct GcHandle<T: Sized>(*mut T);

impl<T: Sized> GcHandle<T>
{

    /// Create a new handle from a raw pointer.
    pub fn from_ptr(ptr: *mut T) -> GcHandle<T>
    {
        GcHandle(ptr)
    }

    /// Check if the handle is null.
    pub fn is_null(&self) -> bool
    {
        self.0.is_null()
    }


    /// Change the type of the handle.
    pub fn cast<U: Sized>(&self) -> GcHandle<U> {
        GcHandle::from_ptr(self.0 as *mut U)
    }

    /// Get the raw pointer.
    pub fn as_ptr(&self) -> *mut T {
        self.0
    }

    /// Applies the given `offset` to the pointer.
    pub fn offset(self, offset: isize) -> GcHandle<T> {
        unsafe { GcHandle::from_ptr(self.0.offset(offset)) }
    }
}

impl<T: Sized> Clone for GcHandle<T> {
    fn clone(&self) -> Self {
        GcHandle(self.0)
    }
}

impl<T: Sized> Copy for GcHandle<T> {}


impl<T: Sized + Debug> Debug for GcHandle<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.0.is_null() {
            write!(f, "GcHandle(null)")
        } else {
            write!(f, "GcHandle({:?})", unsafe { &*self.0 })
        }
    }
}


impl<T: Sized> Default for GcHandle<T>
{
    fn default() -> GcHandle<T>
    {
        GcHandle(std::ptr::null_mut())
    }
}

impl<T: Sized> Deref for GcHandle<T>
{
    type Target = T;

    fn deref(&self) -> &T
    {
        assert!(!self.0.is_null(), "Attempted to dereference a null pointer");
        unsafe { &*self.0 }
    }
}

impl<T: Sized> DerefMut for GcHandle<T>
{
    fn deref_mut(&mut self) -> &mut T
    {
        assert!(!self.0.is_null(), "Attempted to dereference a null pointer");
        unsafe { &mut *self.0 }
    }
}

impl<T: Sized> TryInto<*mut T> for GcHandle<T>
{
    type Error = Error;

    fn try_into(self) -> Result<*mut T, Self::Error> {
        if self.is_null() {
            Err(Error::Simple(ErrorKind::NullPointer))
        } else {
            Ok(self.0)
        }
    }
}

impl<T: Sized> Index<usize> for GcHandle<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        assert!(!self.0.is_null(), "Attempted to dereference a null pointer");
        unsafe { &*self.0.offset(index as isize) }
    }
}


impl<T: Sized> IndexMut<usize> for GcHandle<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        assert!(!self.0.is_null(), "Attempted to dereference a null pointer");
        unsafe { &mut *self.0.offset(index as isize) }
    }
}